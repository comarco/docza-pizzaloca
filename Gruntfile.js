module.exports = function(grunt){
  grunt.initConfig({
    sass: {
      options: {
        loadPath: ['bower_components/foundation-sites/scss']
      },
      dist: {
        options: {
          style: 'nested'
        },
        files: [{
          expand: true,
          cwd: 'bower_components/foundation-sites/scss',
          src: ['*.scss'],
          dest: 'assets/css',
          ext: '.css'
        }]
      }
    },
    watch: {
      css: {
        files: '**/*.scss',
        tasks: ['sass']
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default',['watch']);
};
